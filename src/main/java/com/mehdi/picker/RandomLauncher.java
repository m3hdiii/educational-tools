/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mehdi.picker;

import com.mehdi.grouping.GroupingPanel;

import javax.swing.*;

/**
 * @author mehdi
 */
public class RandomLauncher {
    public static void main(String args[]) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new JFrame("Picker Vanessa");
                //RandomPicker tp = new RandomPicker();
                GroupingPanel gp = new GroupingPanel();
                //frame.add(tp);
                frame.add(gp);

                frame.setSize(1200, 1000);
//                frame.setLayout(null);//using no layout managers
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);//making the frame visible
            }
        });

    }


}
